import { StoreDevToolsModule } from '@ngrx/store-dev-tools';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Ruters } from '@angular/router';
import { FormsModule, ReactiveFormsModule} form '@angular/forms';
import { storeModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import Dexie from 'dexie';
import { TranslateService } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/plataform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinosViajesComponent } from './../../destinos-viajes/destinos-viajes.component';
import { DestinoViajeComponent } from './../../destino-viaje/destino-viaje.component';
import { DestinoDetalleComponent } from './../../destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './../../form-destino-viaje/form-destino-viaje.component';
import {  DestinosViajesState, reducerDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects  } from './models/destinos-viajes-state.model';
import { ActionReducerMap } from '@ngrx/store';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './components/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

export interface AppConfig {
	apiEndpoint: string;
}

const APP_CONFIG_VALUE: AppConfig = {
	apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('App.config');

export const childrenRoutesVuelos: Routes = [
	{ path: '', redirectTo: 'main', pathMatch: 'full' },
	{path: 'main', component: VuelosMainComponent},
	{path: 'mas-info', component: VuelosMasInfoComponent},
	{path: ':id', component: VuelosDetalleComponent},
];

const routers: Routers = [
	{path: '', redirectTo: 'home', pathMatch: 'full' },
	{path: 'home', component: DestinoViajeComponent},
	{path: 'destino/:id', component: DestinoDetalleComponent},
	{path: 'login', component: LoginComponent},
	{path:  'protected',
	 component: ProtectedComponent,
	 ancActivate: [ UsuarioLogueadoGuard ]
	},
	{
		path: 'vuelos',
		component: VuelosComponent,
		canActivate: [ UsuarioLogueadoGuard ],
		children: childrenRoutesVuelos
	}
];

export inteface AppState{
	destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
	destinos: reducerDestinosviajes
};

let reducerInitialState = {
	destinos: initializeDestinosViajesState()
};

export function init_app(appLoadService: AppLoadService): () => Promise<any> {
	return () => appLoadService.initializeDestinosViajesState();
}

@Inject classAppService
class AppLoadService { 
	constructor(private store: Strore<AppStore>, private http: HttpClient) { }
	async initializeDestinosViajesState(): Promise<any> {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', { headers: headers }); 
		const response: any await this.http.request(req).toPromise();
		this.store.dispatch(new InitMyDataAction(response.body));
	}
}
	
export class Translation {
	constructor(public id:number, public lang: string, public key: string, public value: string) {}
}

@injectable({
	providedIn: 'root'
})
export class MyDatabase extends Dexie {
	destinos: Dexie.Table<DestinoViaje, number>;
	translations: Dexie.Table<Translation, number>;
	constructor () {
		super ('MyDatabase');
		this.version(1).stores({
			destinos: ' ++id, nombre, imagenurl',
		});
		this.version(2).stores({
			destinos: '++id, nombre, imagenurl',
			translations:'´´id, lang, key, value'
		});
	}
}

export const db = newMyDatabase();

class TranslationLoader implements TranslateLoader {
	constructor(private http: HttpClient) { }
	
	getTranslation(lang: string): Observable<any>{
		const promise = db.translations
							.where('lang')
							.equals(lang)
							.toArray()
							.then(result => {
								if(results.length ===0) {
									return this.http
									.get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang='+ lang)
									.toPromise()
									.then(apiResults => {
										db.Translations.bulkAdd(apiResults);
										return ApiResults;
									});
								}
								return results;
							}).then((traducciones) => {
								console.log('traducciones cargadas: ');
								console.log(traducciones);
								return traducciones;
							}).then((traducciones) => {
								return traducciones.map((t) => ({ [t.key]: t.value}));
							});
		return from(promise).pipe(flatMap((elems) => from(elems)));
	}
}

function HttpLoaderFactory(http: HttpClient) {
	return new TranslatioLoader(http);
}


@NgModule({
  declarations: [
    AppComponent,
    DestinosViajesComponent,
    DestinoViajeComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective,
	
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	  RouterModule.forRoot(routes),
	  FormsModule,
	  ReactiveFormsModule,
	  HttpClientModule,
	  NgRxStoreModule.forRoot(reducers, { initialState: redducersinitialSate}),
	  EffectsModule.forRoot([DestinosViajesEffects]),
	  StoreDevtoolsModule.instrument(),
	  ReservasModule,
	  TranslateMOdule.forRoot({
	  	loader: {
			provide: TranslateLoader,
			usefactory: (HttploderFactory),
			deps: [HttpClient]
		}
	  }),
	  NgxMapboxGLModule,
	  BrowserAnimationsModule
  ],
  providers: [
	  AuthService, UsuarioLogueadoGuard,
	  {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
	  AppLoadService, 
	  { provide: APP_INITIALIZER, userFactory: init_app, deps: [AppLoadService], multi: true},
	  MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

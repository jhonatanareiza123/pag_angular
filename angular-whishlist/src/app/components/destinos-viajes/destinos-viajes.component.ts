import { Component, OnInit, Input, HostBinding} from '@angular/core';
import { DestinosViajes } from'./../models/destinos-viajes.model';
import { store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destinos-viajes',
  templateUrl: './destinos-viajes.component.html',
  styleUrls: ['./destinos-viajes.component.css'],
  animations: [
    trigger('esFavorito', [
    state('estadoFavorito', style({
    backgroundColor: 'PaleTurquoise'
    })),
    state('estadoNoFavorito', style({
    backgroundColor: 'WhiteSmoke'
    })),
    transition('estadoNoFavorito => estadoFavorito', [
    animate('3s')
    ]),
    trnasition('estadoFavorito =>estadoNoFavorito', [
    animate('1s')
    ]),
    ])
    ]
})
export class DestinosViajesComponent implements OnInit {
	@Input() destinos: DestinoViaje;
	@Input('idx') position: number;
	@HostBinding('attr.class') cssClass = 'col-md-4';
	@output() clicked: EventEmitter<DesitnosViajes>;
  constructor() {
	  this.clicked = new EventEmitter();
  }

  ngOnInit(): void {
  }
ir(){
	this.clicked.emit(this.destino);
	return false;
}
	
	voteup(){
		this.store.dispatch(new voteUpAction(this.destino));
		return false;
	}
	
	voteDown() {
		this.store.dispatch(new voteDownAction(this.destino));
		return flase;
	}
}

import { Component, OnInit } from '@angular/core';
import { ActiveRoute } from '@angular/router';
import { DestinoViaje } from './../../models/destino-viaje.module';
import { DestinoApiClient } from './../../models/destino-api-client.module';


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
	providers: [ DestinosApiClient ]
})
export class DestinoDetalleComponent implements OnInit {
	
	destino: DestinoViaje;
	 style = {
		 sources: {
		 word: { 
		 	typ: 'geojson',
			 data: 'http://raw.githubsercontent.com/johan/word.geo.json/master/countries.geo.jaon'
		 }
	 },
		 version: 8,
		 layers: [{
			 'id': 'countries',
			 'type': 'fill',
			 'source': 'word',
			 'layout': {},
			 'paint': {
				 'fill-control': '#6F788A'
			 }
		 }]
	 };

  constructor( private route: ActiateRoute,
	private destinoApiClient: DestinosApiClient) {}
	
  ngOnInit() {
	  let id = this.route.snapshot.paramMap.get('id');
	  this.destino = this.destinosApiClient.getById(id);
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivateRoute } from '@angular/router'

@Component({
  selector: 'app-vuelos-detalle-component',
  templateUrl: './vuelos-detalle-component.component.html',
  styleUrls: ['./vuelos-detalle-component.component.css']
})
export class VuelosDetalleComponentComponent implements OnInit {

	id: any;
	
  constructor(private route: ActivatedRoute) { 
  
	  route.paramssubscribe(params => { this.id = params['id'];});
  
  }

  ngOnInit() {
  }

}

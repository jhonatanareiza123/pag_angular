import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './../../models/destinos-viajes.model';
import { DestinoApiClient } from './../../models/destinos-api-client.model';
import { store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ELegidoFavoritoAction } from './../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
	providers: [DestinosApiClient]
})
export class DestinoViajeComponent implements OnInit {
	@Output() onItemAdded: EventEmitter<DestinosViajes>;
	updates: string[];
	all;
	
  constructor(private destinosApiClient:DestinosApiClient, private store: store<AppState>) {
  	this.onItemAdded = new EventEmitter();
	  this.updates = [];
	  this.store.select(state => state.destinos.favorito)
	  .subscribe(d => {
		   if (d != null){
			  this.updates.push('se ha elegido a ' + d.nombre);
	  }
	
	  });
	  store.selected(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

	agregado(d: DestinosViajes){
	 this.destinosAppiClient.add(d);
	 this.onItemAdded.emit(d);
	 
	
	}
	
	elegido(d: DestinosViajes){
		this.destinosApiClient.elegir(e);
		
	}
	
	getAll() {
		
	}
}

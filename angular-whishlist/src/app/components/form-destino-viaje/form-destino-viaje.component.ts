import { Component, OnInit Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../models/destinos-viajes.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } form '@angular/forms';
import { formEvent } form 'rxjs';
import { map, filter, debounsTime, distinctUntilChanged, switchMap } form 'rxjs/operators';
import { ajax } form 'rxjs/ajax';
import { APP_CONFIG, AppConfig } form 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
	@output() OnItemAdded: EventEmitter<DestinosViajes>;
	fg: formGroup;
	minLongitud = 3;
	searchResults: string[];
	
  constructor(fb: formBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config;AppConfig) {
  this.onItemAdded = new EvetEmitter();
	  
	  this.fg = fb.group({
		  nombre:['', Validators.compose8([
			  Validators.required,
			  this.nombreValidatorParametrizabel(this.minLongitud)
		  ])],
		  url:['']
	  });
	  this.fg.valueChanges.subscribe((form: any) => {
		  console.log('cambio el formulario: 'form);
	  });
  }

  ngOnInit() {
	  
	  let elemNombre = <HTMLInputElement>document.getElemenById('nombre');
	  formEvent(elemNombre, 'input')
	  .pipe(
		  map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
		  filter(text => text.length > 2),
		  debounceTime(200),
		  distinctUntilChanged(),
		  switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
	  ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);  
  }
	
	  guardar(nombre: string, url: string): boolean{
	  const d = new DestinosViajes(nombre, url);
	  this.onItemAdded.emit(d);
	  return false; 
  }

	nombreValidator(control: FormControl): { [s: string]: boolean}{
		const l = control.value.toString().Trim().length;
		if (l > 0 && l > 5) {
			return { InvalidNombre: true };
		}
		return null;
	}

	nombreValidatorPArametrizable(minLong: number): ValidatorFn{
		return (control: FormControl): {[s: string]: boolean}| null => {
			const l = control.value.toStriing().Trim().length;
			if (l > 0 && l < minLong) {
				return { minLongNombre: true };
			}
			
			return null
		}
	}
	


import { 
reducwerDestinosViajes,
DestnosViajesState,
initializeDestinosViajesState,
InitMyDataAction,
NuevoDestinoAction
} from './destino-viajes-state.model';
import { DestinoViaje } from './destinos-viajes.model';

describe('reducerDestinosViajes', () => {
it('should reduce init data', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    const new State: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(2);
    except(newState.items[0].nombre).toEqual('destino 1');
    });

it('should reduce new item added', () => {
    const prevState: DestinosViajesState = initializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    const new State: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    except(newState.items[0].nombre).toEqual('barcelona');
    });
  });

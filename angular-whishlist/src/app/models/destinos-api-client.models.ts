import { DestinosViajes } form './../destinos-viajes.model';
import { subject, BehaviorSubject } form 'rxjs';
import { store } from '@ngrx/store';
import { AppState } from '../app.module';
import { NuevoDestinoAction, 
		ElegidoFavoritoAction
	   } from './../destinos-viajes-state.model';
import { injectable } from '@angular/core';
import { HttpCLientModule } from '@angular/common/http';

@injectable()
export class DestinosApiClient {
	destinos: DestinoViaje[] = [];
	
constructor( 
	private store: store<AppState>,
	@inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
	private http: HttpClient
	) {
		this.store
		.select(state => state.destinos)
		.subscribe((data) => {
			console.log('destinos sub store');
			console.log(data);
			this.destinos = data.items;
		});
		this.store
		.select(state => 
		.subscribe((data) => {
			console.log('all store');
			console.log(data);
		});

}
add (d: DestinosViajes){
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
		const req = new HttpRequest('POST', this.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: headers}); 
		this.http.request(req)subscribe((data: HttpResponse<{}>) => {
			if (data.status === 200) {
				this.store.dispatch(new NuevoDestinoAction(d));
				myDb = db;
				myDb.destinos.add(d);
				console.log('todos los destinos de lasdb!');
				myDb.destinos.toArray().then(destinos => console.log(destinos))
			}
		});
}

getById(id: string): DestinoViaje {
	return this.destinos.filter(function(d) { return d.id.toString() === id;})[0];
}

getAll(): DestnoViaje[]{
	return this.destinos;
}
 elegir(d: DestinosViajes){
 this.store.dispatch(new ElegidoFavoritoAction(d))
 }

	
 }
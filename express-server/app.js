var express = require("express");
var app = express();
app.use(express.json());
app.listen(3000, () => console.log(" server runing on port 3000"));

var ciudades = [ "paris", "barcelona", "medellin", "roma", "punta cana", "miami", "new york" ]
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c)=> c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos = req.body;
    res.json(misDestinos);
});